// -*- compile-command: "make upload" -*-
// Adapted from https://sccode.org/1-522, which is adapted in
// turn from STK's 'rhodey', which is adapted from a TX81z
// algorithm.

Engine_Rhodes : CroneEngine {
    var <notes;
    var <lfoSpeed;
    var <lfoDepth;
    var <modIndex;
    var <mix;

    *new { arg context, doneCallback;
        ^super.new(context, doneCallback);
    }

    *initClass {
        // TODO store it as a classvar?
        StartUp.add({
           SynthDef("Rhodes",
               {
               | // standard meanings
               out = 0, freq = 440, gate = 1, pan = 0, amp = 0.1,
               // all of these range from 0 to 1
               vel = 0.8, modIndex = 0.1, mix = 0.2,
               lfoSpeed = 0.2, lfoDepth = 0.1
               |
               var env1, env2, env3, env4;
               var osc1, osc2, osc3, osc4, snd;
               freq = freq * 2;
    
               env1 = EnvGen.ar(Env.adsr(0.001, 1.25, 0.0, 0.04, curve: \lin));
               env2 = EnvGen.ar(Env.adsr(0.001, 1.00, 0.0, 0.04, curve: \lin));
               env3 = EnvGen.ar(Env.adsr(0.001, 1.50, 0.0, 0.04, curve: \lin));
               env4 = EnvGen.ar(Env.adsr(0.001, 1.50, 0.0, 0.04, curve: \lin));
    
               osc4 = SinOsc.ar(freq * 0.5) * 2pi * 2 * 0.535887 * modIndex * env4 * vel;
               osc3 = SinOsc.ar(freq, osc4) * env3 * vel;
               osc2 = SinOsc.ar(freq * 15) * 2pi * 0.108819 * env2 * vel;
               osc1 = SinOsc.ar(freq, osc2) * env1 * vel;
               snd = Mix((osc3 * (1 - mix)) + (osc1 * mix));
               snd = snd * (SinOsc.ar(lfoSpeed) * lfoDepth + 1);
    
               snd = snd * EnvGen.ar(Env.asr(0, 1, 0.25), gate);
               snd = Pan2.ar(snd, pan, amp);
               DetectSilence.ar(snd, doneAction: Done.freeSelf);
               Out.ar(out, snd);
           }).add;
        });
    }
    alloc {
        lfoSpeed = 2.0;
        lfoDepth = 0.1;
        modIndex = 0.1;
        mix = 0.2;
        notes = nil!128;
        this.addCommand("note_on", "ii", {
            arg msg;
            var note = msg[1];
            var vel = msg[2];
            if(notes[note].isNil) {
                notes[note] = Synth("Rhodes",
                                    [
                                        \out, context.out_b,
                                        \freq, note.midicps,
                                        \vel, vel/127.0,
                                        \lfoSpeed, lfoSpeed,
                                        \modIndex, modIndex,
                                        \lfoDepth, lfoDepth,
                                        \mix, mix
                                    ],
                                    target: context.xg);
            }
        });

        this.addCommand("note_off", "f", {
            arg msg;
            var note = msg[1];
            if(notes[note].notNil) {
                notes[note].release;
                notes[note] = nil;
            }
        });
        this.addCommand("set_lfo_speed", "f", {
            arg msg;
            lfoSpeed = msg[1];
            // there has to be a better way to do this
            for (0, 128, {
                arg i;
                if (notes[i].notNil) {
                    notes[i].set(\lfoSpeed, msg[1]);
                }
            })
        });
        this.addCommand("set_lfo_depth", "f", {
            arg msg;
            lfoDepth = msg[1];
            // there has to be a better way to do this
            for (0, 128, {
                arg i;
                if (notes[i].notNil) {
                    notes[i].set(\lfoDepth, msg[1]);
                }
            })
        });
        this.addCommand("set_mod_index", "f", {
            arg msg;
            modIndex = msg[1];
            // there has to be a better way to do this
            for (0, 128, {
                arg i;
                if (notes[i].notNil) {
                    notes[i].set(\modIndex, msg[1]);
                }
            })
        });
        this.addCommand("set_mix", "f", {
            arg msg;
            mix = msg[1];
            // there has to be a better way to do this
            for (0, 128, {
                arg i;
                if (notes[i].notNil) {
                    notes[i].set(\mix, msg[1]);
                }
            })
        });
    }
    free {
    }
}