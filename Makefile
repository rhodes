
all:

upload: rhodes.lua lib/Engine_Rhodes.sc rhodes.png
	find . -not -path '*/\.*' -type f | xargs -P 5 -n 1 norns_upload gretchen/rhodes
	norns_load gretchen/rhodes rhodes.lua

.PHONY: all upload

